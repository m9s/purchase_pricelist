#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Purchase Pricelist',
    'name_de_DE': 'Einkauf Preislisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Define pricelists for puchases
''',
    'description_de_DE': '''
    - Ermöglicht die Verwendung von Preislisten im Modul Einkauf
''',
    'depends': [
        'purchase',
        'account_invoice_pricelist'
    ],
    'xml': [
        'purchase.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
