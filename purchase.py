#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Equal, Get
from trytond.transaction import Transaction
from trytond.pool import Pool


STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft'))}
DEPENDS = ['state']

class Purchase(ModelSQL, ModelView):
    "Purchase"
    _description = __doc__
    _name = "purchase.purchase"

    pricelist = fields.Many2One('pricelist.pricelist', 'Pricelist',
            on_change=['pricelist'], states=STATES, required=True,
            context={'pricelist_type': 'purchase'}, domain=[
                ('active', '=', True),
                ('type', '=', 'purchase'),
                ('company', '=', Eval('company')),
            ], depends=DEPENDS+['company'])

    def on_change_pricelist(self, vals):
        pricelist_obj = Pool().get('pricelist.pricelist')
        res = {}
        if vals.get('pricelist'):
            pricelist = pricelist_obj.browse(vals['pricelist'])
            if not pricelist.active==True:
                self.raise_user_error('pricelist_no_currency')
            else:
                res['currency'] = pricelist.currency.id
        return res

    def on_change_party(self, vals):
        pool = Pool()
        party_obj = pool.get('party.party')
        address_obj = pool.get('party.address')
        payment_term_obj = pool.get('account.invoice.payment_term')
        res = {
            'invoice_address': False,
            'payment_term': False,
            'pricelist': False
        }
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            res['invoice_address'] = party_obj.address_get(party.id,
                    type='invoice')
            if party.pricelist_purchase:
                if party.pricelist_purchase.active==True:
                    res['pricelist'] = party.pricelist_purchase.id
                else:
                    self.raise_user_warning('inactive_pricelist',
                            'cust_list_inactive')
            if party.supplier_payment_term:
                res['payment_term'] = party.supplier_payment_term.id

        if res['invoice_address']:
            res['invoice_address.rec_name'] = address_obj.browse(
                    res['invoice_address']).rec_name
        if not res['payment_term']:
            res['payment_term'] = self.default_payment_term()
        if res['payment_term']:
            res['payment_term.rec_name'] = payment_term_obj.browse(
                    res['payment_term']).rec_name
        return res

    def create_invoice(self, purchase_id):
        '''
        Create invoice for the purchase id

        :param purchase_id: the id of the purchase
        :return: the id of the invoice or None
        '''
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        journal_obj = pool.get('account.journal')
        invoice_line_obj = pool.get('account.invoice.line')
        purchase_line_obj = pool.get('purchase.line')

        context = {}
        purchase = self.browse(purchase_id)
        invoice_lines = self._get_invoice_line_purchase_line(purchase)
        if not invoice_lines:
            return

        journal_id = journal_obj.search([
            ('type', '=', 'expense'),
            ], limit=1)
        if journal_id:
            journal_id = journal_id[0]

        context['user'] = Transaction().user
        with Transaction().set_user(0):
            with Transaction().set_context(**context):
                invoice_id = invoice_obj.create({
                    'company': purchase.company.id,
                    'type': 'in_invoice',
                    'reference': purchase.reference,
                    'journal': journal_id,
                    'party': purchase.party.id,
                    'invoice_address': purchase.invoice_address.id,
                    'currency': purchase.currency.id,
                    'account': purchase.party.account_payable.id,
                    'payment_term': purchase.payment_term.id,
                    'pricelist': purchase.pricelist.id,
                })

        for line_id in invoice_lines:
            for vals in invoice_lines[line_id]:
                vals['invoice'] = invoice_id
                with Transaction().set_user(0):
                    with Transaction().set_context(**context):
                        invoice_line_id = invoice_line_obj.create(vals)

                purchase_line_obj.write(line_id, {
                    'invoice_lines': [('add', invoice_line_id)],
                    })

        with Transaction().set_user(0):
            with Transaction().set_context(**context):
                invoice_obj.update_taxes([invoice_id])

        self.write(purchase_id, {
            'invoices': [('add', invoice_id)],
        })
        return invoice_id

Purchase()


class PurchaseLine(ModelSQL, ModelView):
    "Purchase Line"
    _description = __doc__
    _name = "purchase.line"

    def __init__(self):
        super(PurchaseLine, self).__init__()
        fields = [self.product, self.quantity, self.unit]
        for field in fields:
            field = copy.copy(field)
            if field.on_change is None:
                field.on_change = []
            if '_parent_purchase.pricelist' not in field.on_change:
                field.on_change += ['_parent_purchase.pricelist']

        if not self.product.context:
            self.product.context = {
                    'pricelist': Get(Eval('_parent_purchase', {}),
                        'pricelist'),
                    'currency': Get(Eval('_parent_purchase', {}),
                        'currency'),}
        else:
            if 'pricelist' not in self.product.context:
                self.product.context['pricelist'] = \
                        Get(Eval('_parent_purchase', {}), 'pricelist')
            if 'currency' not in self.product.context:
                self.product.context['currency'] = \
                        Get(Eval('_parent_purchase', {}), 'currency')

        self._reset_columns()

    def on_change_product(self, vals):
        product_obj = Pool().get('product.product')
        pricelist_obj = Pool().get('pricelist.pricelist')

        res = super(PurchaseLine, self).on_change_product(vals)
        context = {}
        if not vals.get('product'):
            return res

        product = product_obj.browse(vals['product'])

        if vals.get('_parent_purchase.currency'):
            context['currency'] = vals['_parent_purchase.currency']
        if vals.get('unit'):
            context['uom'] = vals['unit']
        else:
            context['uom'] = product.purchase_uom.id

        if vals.get('_parent_purchase.pricelist'):
            context['pricelist'] = vals['_parent_purchase.pricelist']

        with Transaction().set_context(**context):
            res['unit_price'] = pricelist_obj.get_price([product.id],
                    quantity=vals.get('quantity', 1))[product.id]

        vals = vals.copy()
        vals['unit_price'] = res['unit_price']
        vals['type'] = 'line'
        res['amount'] = self.on_change_with_amount(vals)
        return res

    def on_change_quantity(self, vals):
        product_obj = Pool().get('product.product')
        pricelist_obj = Pool().get('pricelist.pricelist')

        res = super(PurchaseLine, self).on_change_quantity(vals)
        context = {}
        if not vals.get('product'):
            return res

        product = product_obj.browse(vals['product'])

        if vals.get('_parent_purchase.currency'):
            context['currency'] = vals['_parent_purchase.currency']
        if vals.get('_parent_purchase.party'):
            context['supplier'] = vals['_parent_purchase.party']
        if vals.get('unit'):
            context['uom'] = vals['unit']
        else:
            context['uom'] = product.purchase_uom.id

        if vals.get('_parent_purchase.pricelist'):
            context['pricelist'] = vals.get('_parent_purchase.pricelist')

        with Transaction().set_context(**context):
            res['unit_price'] = pricelist_obj.get_price([product.id],
                    quantity=vals.get('quantity', 1))[product.id]
        return res

PurchaseLine()
